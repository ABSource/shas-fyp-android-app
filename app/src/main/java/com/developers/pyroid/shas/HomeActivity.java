package com.developers.pyroid.shas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.developers.pyroid.shas.DTO.Classes.Notification;
import com.developers.pyroid.shas.DTO.Classes.NotificationCollection;
import com.developers.pyroid.shas.DTO.Classes.NotificationCommInterface;
import com.developers.pyroid.shas.Fragments.ImageViewFragment;
import com.developers.pyroid.shas.Fragments.LockHandlerFragment;
import com.developers.pyroid.shas.Fragments.ManageUserFragment;
//import com.developers.pyroid.shas.Fragments.NewUserFragment;
import com.developers.pyroid.shas.Fragments.NotificationsFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class HomeActivity extends AppCompatActivity{

    private boolean admin;
    private BottomNavigationView navBar;
    private ImageViewFragment imViewFragment;
    private NotificationsFragment notifyFragment;
//    private NewUserFragment newUserFragment;
    private LockHandlerFragment lockHandlerFragment;
    private ManageUserFragment manageUserFragment;
    private MenuItem newUser, manageUsers;
    private SharedPreferences sharedPref;
    private String uName, uPassword;
    private static final String TAG = "HomeActivity";
    private ArrayList<Notification> notificationList;
    NotificationCollection nc;
    private int isAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        sharedPref = getSharedPreferences(getString(R.string.SHARED_PREF), MODE_PRIVATE);
        isAdmin = sharedPref.getInt("isAdmin", 0);
        if(isAdmin == 1)
            admin = true;
        else
            admin = false;


        nc = new NotificationCollection();
        notificationList = nc.getNotifications();
        navBar = (BottomNavigationView) findViewById(R.id.id_bottom_nav);



        imViewFragment = new ImageViewFragment();
        notifyFragment = new NotificationsFragment();

//        newUserFragment = new NewUserFragment();
        manageUserFragment = new ManageUserFragment();
        lockHandlerFragment = new LockHandlerFragment();

        Bundle notif_list_bundle = new Bundle();
        Collections.reverse(notificationList);
        notif_list_bundle.putParcelableArrayList("Notification",notificationList);
        notifyFragment.setArguments(notif_list_bundle);

        setFragment(notifyFragment);
        navBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.id_nav_notify:
                        setFragment(notifyFragment);
                        return true;
                    case R.id.id_nav_cam:
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
                        String datetime = dateformat.format(c.getTime());
                        String [] dateandtime= datetime.split(" ");
                        String date = dateandtime[0];
                        String time = dateandtime[1];
                        Notification notification = new Notification(1,R.drawable.image,date,time,"Muhammad Ali");
                        Collections.reverse(notificationList);
                        notificationList.add(notification);
                        Collections.reverse(notificationList);
                        setFragment(imViewFragment);
                        return true;
                    case R.id.id_nav_lock:
                        setFragment(lockHandlerFragment);
                        return true;
                    default:

                        return false;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_items,menu);

        if(!admin){
            newUser = menu.findItem(R.id.id_action_new_user);
            manageUsers = menu.findItem(R.id.id_action_manage_user);
            this.invalidateOptionsMenu();
            newUser.setVisible(false);
            manageUsers.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.id_action_new_user:
                startActivity(new Intent(HomeActivity.this, SignUpRequestActivity.class));
                return true;
            case R.id.id_action_account:
                startActivity(new Intent(HomeActivity.this, Main2Activity.class));
                return true;
            case R.id.id_action_manage_user:
                startActivity(new Intent(HomeActivity.this, ManageUserActivity.class));
                return true;
            case R.id.id_action_sign_out:
                startActivity(new Intent(HomeActivity.this,LogInActivity.class));
                return true;
            default:
                return false;
        }
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.id_home_frame,fragment);
        fragmentTransaction.commit();
    }
}
