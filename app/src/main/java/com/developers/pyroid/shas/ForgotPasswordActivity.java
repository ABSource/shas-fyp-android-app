package com.developers.pyroid.shas;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends AppCompatActivity {
    Button setPasswordBtn;
    EditText securityAnsTxt;
    TextView errorTxt;
    com.xwray.passwordview.PasswordView newPasswordTxt;
    org.angmarch.views.NiceSpinner niceSpinner;
    Spinner secQuestionSpinner;
    static final String SUCCESS = "success";
    static final String MESSAGE = "message";
    private String _url = "http://192.168.0.100/shas/reset_password.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_forgot_password);
//        niceSpinner = (org.angmarch.views.NiceSpinner) findViewById(R.id.securityQuestionSpinner);
        secQuestionSpinner = (Spinner)findViewById(R.id.secQuesSpinner);
        securityAnsTxt = (EditText) findViewById(R.id.input_security_answer);
        newPasswordTxt = (com.xwray.passwordview.PasswordView) findViewById(R.id.input_password_new);
        setPasswordBtn = (Button) findViewById(R.id.btn_resetPassword);
        errorTxt = (TextView) findViewById(R.id.errorTxtViewRestPass);
//        List<String> dataset = new LinkedList<>(Arrays.asList("Select your security question...",
//                "Your First Pet Name",
//                "Your elementary school name",
//                "Your favorite sports team",
//                "Your best friend's nick name",
//                "Street name you grow up on"));
//        niceSpinner.attachDataSource(dataset);
//        niceSpinner.addTextChangedListener(new ForgotPasswordActivity.MyTextWatcher(niceSpinner));
        ArrayAdapter SecQuesadapter = ArrayAdapter.createFromResource(this, R.array.securityQuestionArray, android.R.layout.simple_spinner_item);
        // Set the layout to use for each dropdown item
        SecQuesadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        secQuestionSpinner.setAdapter(SecQuesadapter);

        secQuestionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String country = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        securityAnsTxt.addTextChangedListener(new ForgotPasswordActivity.MyTextWatcher(securityAnsTxt));
        newPasswordTxt.addTextChangedListener(new ForgotPasswordActivity.MyTextWatcher(newPasswordTxt));
        setPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
    }

    private void submitForm() {
        if(!validateQuestion())
            return;
        if (!validateSecurityAns()) {
            return;
        }
        if (!validateNewPassword()) {
            return;
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, _url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    if(json.getInt(SUCCESS) > 0) {
                        startActivity(new Intent(ForgotPasswordActivity.this,LogInActivity.class));
                    }
                    Toast.makeText(ForgotPasswordActivity.this, json.getString(MESSAGE), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ForgotPasswordActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("password",newPasswordTxt.getText().toString());
                params.put("question",secQuestionSpinner.getSelectedItem().toString());
                params.put("answer",securityAnsTxt.getText().toString());

                return params;
            }
        };
        //Adding stringRequest to requestQueue
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private boolean validateSecurityAns() {
        String ans = securityAnsTxt.getText().toString();

        if (ans.isEmpty()) {
            //securityAnsTxt.setError(getString(R.string.vallidSecurityAns));
            errorTxt.setText(R.string.vallidSecurityAns);
            errorTxt.setVisibility(View.VISIBLE);
            securityAnsTxt.requestFocus();
            return false;
        } else {
            errorTxt.setVisibility(View.INVISIBLE);
        }
        return true;
    }

    private boolean validateNewPassword() {
        if (newPasswordTxt.getText().toString().trim().isEmpty()) {
            //newPasswordTxt.setError(getString(R.string.err_msg_password));
            //newPasswordTxt.setError(getString(R.string.err_msg_password));
            errorTxt.setText(R.string.err_msg_password);
            errorTxt.setVisibility(View.VISIBLE);
            requestFocus(newPasswordTxt);
            return false;
        } else {

            errorTxt.setVisibility(View.INVISIBLE);

        }
        return true;
    }
    private boolean validateQuestion(){
        if(secQuestionSpinner.getSelectedItem().toString().equals("Select your security question...")){
            errorTxt.setText(R.string.vallidSecurityQuestion);
            errorTxt.setVisibility(View.VISIBLE);
            return false;
        }
        else
            errorTxt.setVisibility(View.INVISIBLE);
        return true;
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.secQuesSpinner:
                    validateQuestion();
                    break;
                case R.id.input_security_answer:
                    validateSecurityAns();
                    break;
                case R.id.resetPasswordText:
                    validateNewPassword();
                    break;
            }
        }
    }

    public static class AccountsActivity extends AppCompatActivity {
        ImageView secQuestionEditBtn;
        TextView existingQuestion;
        EditText oldAns;
        Spinner newQuestSpinner;
        EditText newAns;
        Button saveBtn;
        Button cancelBtn;
        TextView nameText;
        TextView textemail;
        TextView password;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_accounts);
            secQuestionEditBtn =(ImageView) findViewById(R.id.secQuestionEditBtn);
            secQuestionEditBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder setSecurityQues = new AlertDialog.Builder(AccountsActivity.this);
                    View set_qest_view = getLayoutInflater().inflate(R.layout.set_security_question,null);
    //                nameText = (TextView)findViewById(R.id.fullNameProfile);
    //                textemail = (TextView)findViewById(R.id.emailProfile);
    //                password = (TextView) findViewById(R.id.profilepasswordText);
    //                existingQuestion = (TextView)findViewById(R.id.existing_security_question);
    //                oldAns = (EditText)findViewById(R.id.old_security_ans);
    //                newQuestSpinner = (Spinner)findViewById(R.id.securityQuestionSpinner);
    //                saveBtn = (Button) findViewById(R.id.profileSaveBtn);
    //                cancelBtn = (Button)findViewById(R.id.profilecancelbtn);
                    setSecurityQues.setView(set_qest_view);
                    AlertDialog dialog = setSecurityQues.create();
                    dialog.show();
                }
            });
        }
    }
}