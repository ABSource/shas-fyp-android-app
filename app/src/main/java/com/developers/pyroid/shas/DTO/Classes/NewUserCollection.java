package com.developers.pyroid.shas.DTO.Classes;

import java.util.ArrayList;
import com.developers.pyroid.shas.R;
/**
 * Created by Bilal on 3/18/2018.
 */

public class NewUserCollection{
    public ArrayList<SignupRequest> getNewUsers(SignupRequest[] requests){
        ArrayList<SignupRequest> newUserList = new ArrayList<>();

        for(int i=0; i<requests.length; i++){
            newUserList.add(requests[i]);
        }
        return newUserList;
    }
}
