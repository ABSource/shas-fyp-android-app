package com.developers.pyroid.shas.DTO.Classes;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Mohammad Ali on 2/25/2018.
 */

public class Notification implements Parcelable{
    int id;
    Integer image;
    String date, time, userName;

    public Notification(int id,Integer image, String date, String time, String userName) {
        this.image = image;
        this.date = date;
        this.time = time;
        this.userName = userName;
        this.id = id;
    }

    protected Notification(Parcel in) {
        id = in.readInt();
        date = in.readString();
        time = in.readString();
        userName = in.readString();
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public Notification() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(date);
        parcel.writeString(time);
        parcel.writeString(userName);
    }
}
