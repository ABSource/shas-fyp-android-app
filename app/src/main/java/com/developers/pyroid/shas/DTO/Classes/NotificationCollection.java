package com.developers.pyroid.shas.DTO.Classes;

import com.developers.pyroid.shas.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Bilal on 3/6/2018.
 */

public class NotificationCollection{
    ArrayList<Notification> notificationsList;

    public NotificationCollection() {
        this.notificationsList = new ArrayList<>();
    }

    public ArrayList<Notification> getNotifications(){
        notificationsList= new ArrayList<>();
        Notification notification = new Notification(1,R.drawable.image2,"12/3/2012","02:12","Muhammad Bilal");
        notificationsList.add(notification);
        notification = new Notification(2,R.drawable.image2,"22/03/2012","04:12","Muhammad Ali");
        notificationsList.add(notification);

        notification = new Notification(3,R.drawable.image2,"05/03/2012","12:22","Muhammad Haroon");
        notificationsList.add(notification);

        notification = new Notification(4,R.drawable.image2,"12/03/2012","02:12","Muhammad Bilal");
        notificationsList.add(notification);

        notification = new Notification(5,R.drawable.image2,"12/03/2012","02:12","Muhammad Bilal");
//        notificationsList.add(notification);
//
//        notification = new Notification(6,R.drawable.image2,"01/03/2018","02:12","Muhammad Basharat");
//        notificationsList.add(notification);
//
//        notification = new Notification(7,R.drawable.image2,"22/03/2012","04:12","Muhammad Ali");
//        notificationsList.add(notification);
//
//        notification = new Notification(8,R.drawable.image2,"05/03/2012","12:22","Muhammad Haroon");
//        notificationsList.add(notification);
//
//        notification = new Notification(9,R.drawable.image2,"12/03/2012","02:12","Muhammad Bilal");
//        notificationsList.add(notification);
//
//        notification = new Notification(10,R.drawable.image2,"12/03/2012","02:12","Muhammad Bilal");
//        notificationsList.add(notification);

        notification = new Notification(11,R.drawable.image2,"01/03/2018","02:12","Muhammad Basharat");
        notificationsList.add(notification);

        return notificationsList;

    }
    public void addNotification(Notification notification){
        notificationsList.add(notification);
    }

}
