package com.developers.pyroid.shas.Fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.developers.pyroid.shas.DTO.Classes.Notification;
import com.developers.pyroid.shas.DTO.Classes.NotificationCollection;
import com.developers.pyroid.shas.R;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Stack;


public class NotificationsFragment extends Fragment {

    FloatingActionMenu fam;
    FloatingActionButton fabSearchBtn, fabDeleteBtn;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    int _year, _month, _day;   //Date for Searching/Deleting Notifications
    boolean isSearchBtnPressed, isDeleteBtnPressed;
    NotificationAdapter notifyAdapter;
    Activity context;

    ImageView notifyImg;
    TextView nameUser,dateCapture,timeCapture;
    View imageViewer;

    ListView listView;
    NotificationAdapter notificationAdapter;
    ArrayList<Notification> notificationList;
    private Notification notification;

    public NotificationsFragment(){
        // Required empty public constructor
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_notifications, container, false);
        Bundle args = this.getArguments();
        if(args!= null){
            notificationList = args.getParcelableArrayList("Notification");
//            Collections.reverse(notificationList);
        }

        return v;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();

        listView = (ListView) view.findViewById(R.id.mainnotificationListView);

        notificationClickListner();

        floatingActionMenu(view);

        notifyAdapter = new NotificationAdapter(notificationList);
        listView.setAdapter(notifyAdapter);

        searchandDeleteHandler();
    }
    // show dialog on item click
    private void notificationClickListner(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            AlertDialog.Builder imageViewerBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater =  LayoutInflater.from(context);
            imageViewer = context.getLayoutInflater().inflate(R.layout.notifaction_image_layout,null);
            imageViewerBuilder.setView(imageViewer);

            notifyImg = (ImageView) imageViewer.findViewById(R.id.notification_img);
            nameUser = (TextView)imageViewer.findViewById(R.id.UserNameCaptureImage);
            dateCapture = (TextView)imageViewer.findViewById(R.id.dateCapture);
            timeCapture = (TextView)imageViewer.findViewById(R.id.captureTime);

            //add notification item list
            notifyImg.setImageResource(notificationList.get(i).getImage());
            nameUser.setText(notificationList.get(i).getUserName());
            dateCapture.setText(notificationList.get(i).getDate());
            timeCapture.setText(notificationList.get(i).getTime());
            imageViewerBuilder.show();
            }
        });
    }
    //show floating action menu

    private void floatingActionMenu(View view){
        fam = (FloatingActionMenu) view.findViewById(R.id.do_fam);
        fabSearchBtn = (FloatingActionButton) view.findViewById(R.id.fab_search);
        fabDeleteBtn = (FloatingActionButton) view.findViewById(R.id.fab_delete);
    }

    //handle search and delete floating menu item and filtering
    private void searchandDeleteHandler(){
        mDateSetListener = new DatePickerDialog.OnDateSetListener(){    //On Date Selected or Ok Button is pressed
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = day + "/" + month + "/" + year;
                if(date.indexOf("/")==1 && date.lastIndexOf("/")!=4)
                {
                    date = "0"+date.substring(0,1)+date.substring(1,2)+"0"+date.substring(2,8);
                }
                else if(date.indexOf("/")==1 && date.lastIndexOf("/")==4){
                    date = "0"+date.substring(0,1)+date.substring(1,2)+date.substring(2,9);

                }
                else if(date.indexOf("/")!=1 && date.lastIndexOf("/")==4){
                    date = date.substring(0,1)+date.substring(1,3)+"0"+date.substring(3,9);
                }
                if(isSearchBtnPressed) {
                    filterNotification(date);

                }
                else if(isDeleteBtnPressed)
                {
                    deleteNotifications(notificationList,date);
                }



            }
        };
        fabSearchBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TODO something when floating action menu first item clicked
                displayDatePicker();
                isSearchBtnPressed= true;
                isDeleteBtnPressed = false;
            }
        });

        fabDeleteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TODO something when floating action menu second item clicked
                displayDatePicker();
                isDeleteBtnPressed = true;
                isSearchBtnPressed= false;
            }
        });
    }

    // show all notification of given date
    private void filterNotification(String date){
        ArrayList<Notification> searchList = new ArrayList<>();
        for(Notification notif:notificationList) {
            if (notif.getDate().equals(date))
                searchList.add(notif);
        }
        notifyAdapter.setNotificationList(searchList);
        listView.setAdapter(notifyAdapter);
    }

    public void addNotification(Notification notification){
        notificationList.add(notification);
        NotificationAdapter notificationAdapter = new NotificationAdapter(notificationList);
        listView.setAdapter(notificationAdapter);
    }

    // delete notification of given date
    private void deleteNotifications(ArrayList<Notification>list, String date){

        int i = 0, size = list.size();
        for( ; i < list.size() ;){
            if(list.get(i).getDate().equals(date))
            {
                list.remove(i);
                i = 0;
            }
            else
                i++;
        }
        if(list.size() != 0)
        {
            notifyAdapter.setNotificationList(list);
            notifyAdapter.notifyDataSetChanged();
        }
        if(size == list.size())
            Toast.makeText(context,"No notification found at given date!",Toast.LENGTH_SHORT).show();

    }
    private int getItemPosition(ArrayList<Notification> list, String key){
        Collections.reverse(list);
        for(Notification notif:list){
            if(notif.getDate().equals(key))
                return list.indexOf(notif);
        }
        return -1;
    }
    private void displayDatePicker(){
        Calendar cal = Calendar.getInstance();
        _year = cal.get(Calendar.YEAR);
        _month = cal.get(Calendar.MONTH);
        _day = cal.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(
                getActivity(),
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mDateSetListener,
                _year, _month, _day);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    
    class NotificationAdapter extends BaseAdapter {
        ArrayList<Notification> notificationList;

        public NotificationAdapter(ArrayList<Notification> notificationList) {
            this.notificationList = notificationList;
        }

        public ArrayList<Notification> getNotificationList() {
            return notificationList;
        }

        public void setNotificationList(ArrayList<Notification> notificationList) {
            this.notificationList = notificationList;
        }

        @Override
        public int getCount() {
            return notificationList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.main_notification_list_item, null);
            TextView uName = v.findViewById(R.id.item_image_name);
            TextView date = v.findViewById(R.id.notification_item_date);
            TextView time = v.findViewById(R.id.notification_item_time);
            ImageView notificationImg = v.findViewById(R.id.item_image_notification);
            notificationImg.setImageResource(notificationList.get(i).getImage());
            uName.setText(notificationList.get(i).getUserName());
            date.setText(notificationList.get(i).getDate());
            time.setText(notificationList.get(i).getTime());
            return v;
        }
    }



}
