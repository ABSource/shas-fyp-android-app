package com.developers.pyroid.shas;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LogInActivity extends AppCompatActivity {

    Context context;
    private EditText inputEmail, inputPassword;
    //private TextInputLayout inputLayoutEmail, inputLayoutPassword;
    private Button btnSignIn;
    private TextView haveNotAccountText, forgotpasswordText , errorTxt;
    private SharedPreferences sharedPrefLogin;
    private SharedPreferences.Editor editor;

    private static final String SUCCESS = "success";
    private static final String MESSAGE = "message";
    private String url_login_user = "http://192.168.0.100/shas/login_user.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_log_in);


     //   inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
     //   inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);


        inputEmail = (EditText) findViewById(R.id.input_email_login);
        inputPassword = (EditText) findViewById(R.id.input_password_login);
        errorTxt = (TextView)findViewById(R.id.errorTxtViewLogin);
        btnSignIn = (Button) findViewById(R.id.btn_signin);
        sharedPrefLogin = getSharedPreferences(getString(R.string.SHARED_PREF), MODE_PRIVATE);

        inputEmail.addTextChangedListener(new MyTextWatcher(inputEmail));
        inputPassword.addTextChangedListener(new MyTextWatcher(inputPassword));
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
        haveNotAccountText = (TextView)findViewById(R.id.have_not_account);
        haveNotAccountText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LogInActivity.this, SignupActivity.class));
            }
        });
        forgotpasswordText = (TextView)findViewById(R.id.getNew);
        forgotpasswordText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LogInActivity.this,ForgotPasswordActivity.class));
            }
        });
    }
    private void submitForm() {
        if (!validateEmail()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }
        String response;
        final String email = inputEmail.getText().toString();
        final String password = inputPassword.getText().toString();
        editor = sharedPrefLogin.edit();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_login_user, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    if(json.getInt(SUCCESS) == 0)
                        Toast.makeText(LogInActivity.this, json.getString(MESSAGE), Toast.LENGTH_LONG).show();
                    else if(json.getInt(SUCCESS) == 2){
                        startActivity(new Intent(getApplicationContext(),PendingLoginActivity.class));
                    }
                    else{
                        int uID = json.getInt("uID");
                        String uName = json.getString("name");
                        int isAdmin = json.getInt("isAdmin");
                        String question = json.getString("question");
                        String answer = json.getString("answer");

                        editor.putInt("uID", uID);
                        editor.putString("name", uName);
                        editor.putString("email", inputEmail.getText().toString());
                        editor.putString("password", inputPassword.getText().toString());
                        editor.putInt("isAdmin", isAdmin);
                        editor.putString("question", question);
                        editor.putString("answer", answer);

                        editor.commit();

                        startActivity(new Intent(LogInActivity.this, HomeActivity.class));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LogInActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        //Adding stringRequest to requestQueue
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }


    private boolean validateEmail() {
        String email = inputEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            //inputEmail.setError(getString(R.string.err_msg_email));
            errorTxt.setText(R.string.err_msg_email);
            errorTxt.setVisibility(View.VISIBLE);
            requestFocus(inputEmail);
            return false;
        } else {
            errorTxt.setVisibility(View.INVISIBLE);

        }

        return true;
    }
    private boolean validatePassword() {
        if (inputPassword.getText().toString().trim().isEmpty()) {
            //inputPassword.setError(getString(R.string.err_msg_password));
            errorTxt.setText(R.string.err_msg_password);
            errorTxt.setVisibility(View.VISIBLE);
            requestFocus(inputPassword);
            return false;
        } else {

        }

        return true;
    }
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_email_login:
                    validateEmail();
                    break;
                case R.id.input_password_login:
                    validatePassword();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Do you want to quit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finishAffinity();
                    }
                })
                .setNegativeButton("No",null)
                .show();
    }
}
