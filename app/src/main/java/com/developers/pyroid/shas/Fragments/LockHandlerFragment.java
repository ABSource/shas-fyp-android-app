package com.developers.pyroid.shas.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.developers.pyroid.shas.R;

import pl.droidsonroids.gif.GifDrawable;

public class LockHandlerFragment extends Fragment {
    ImageView imageView;
    private OnFragmentInteractionListener mListener;

    public LockHandlerFragment() {
        // Required empty public constructor
    }
    public static LockHandlerFragment newInstance(String param1, String param2) {
        LockHandlerFragment fragment = new LockHandlerFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lock_handler, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        imageView = (ImageView)getActivity().findViewById(R.id.gif_image);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.setImageResource(R.drawable.lock);
                final Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    public void run() {
                        imageView.setImageResource(R.drawable.unlock4);
                    }
                };
                handler.postDelayed(runnable, 1800); //for initial delay..
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
