package com.developers.pyroid.shas.AdapterClasses;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developers.pyroid.shas.DTO.Classes.Notification;
import com.developers.pyroid.shas.R;

import java.util.ArrayList;

/**
 * Created by Bilal on 3/6/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationHolder> {
   Context context;
    ArrayList<Notification> notificationArrayList;


    public NotificationAdapter(Context context , ArrayList<Notification> notificationArrayList){
        this.context = context;
        this.notificationArrayList = notificationArrayList;

    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent ,int viewType){
        View  viewNotification = LayoutInflater.from(parent.getContext()).inflate(R.layout.notifaction_image_layout,parent,false);
        NotificationHolder notificationHolder = new NotificationHolder(viewNotification);
        return notificationHolder;
    }

    @Override
    public void onBindViewHolder(NotificationHolder holder, int position) {
        holder.userNameTxt.setText(notificationArrayList.get(position).getUserName());
        holder.dateTxt.setText(notificationArrayList.get(position).getDate());
        holder.timeTxt.setText(notificationArrayList.get(position).getTime());
        holder.notifImage.setImageResource(notificationArrayList.get(position).getImage());

    }

    @Override
    public int getItemCount() {
        return notificationArrayList.size();
    }
}
