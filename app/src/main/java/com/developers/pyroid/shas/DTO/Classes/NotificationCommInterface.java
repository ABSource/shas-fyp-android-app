package com.developers.pyroid.shas.DTO.Classes;

import java.util.ArrayList;

/**
 * Created by Bilal on 3/28/2018.
 */

public interface NotificationCommInterface {
    public void passNotificationToFragment(ArrayList<Notification> notifications);
}
