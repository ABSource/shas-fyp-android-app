package com.developers.pyroid.shas;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Bilal on 2/28/2018.
 */

public class CardViewItemActivity extends AppCompatActivity {
    ImageView itemimage;
    TextView date;
    TextView time;
    TextView user;
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.list_item);
        itemimage = (ImageView)findViewById(R.id.item_image_notification);
        date = (TextView)findViewById(R.id.notification_item_date);
        time = (TextView)findViewById(R.id.notification_item_time);
        user = (TextView)findViewById(R.id.item_image_name);
        int imageResource = getResources().getIdentifier("@drawable/image",null,this.getPackageName());
        itemimage.setImageResource(imageResource);
    }
}
