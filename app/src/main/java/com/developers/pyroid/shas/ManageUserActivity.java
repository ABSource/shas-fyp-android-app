package com.developers.pyroid.shas;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.developers.pyroid.shas.DTO.Classes.User;
import com.developers.pyroid.shas.DTO.Classes.UserCollection;
import com.developers.pyroid.shas.Fragments.ManageUserFragment;

import java.util.ArrayList;
import java.util.List;

public class ManageUserActivity extends AppCompatActivity {

    TextView nameTxt,checkbox_name, mng_email,checkbox_email;

    ImageView delete_user;
    ArrayList<User> Users, usersList;
    ListView userListView;
    View v , user_alert_view;
    Activity context;
    CheckBox isAdmin_checkbox;
    ManageUserAdapter manageUserAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_user);

        getSupportActionBar().setTitle("Manage User");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        userListView = (ListView)findViewById(R.id.manageUserListView);
        UserCollection userCollection = new UserCollection();
        usersList = userCollection.getAllUsers();
        manageUserAdapter = new ManageUserAdapter();
        userListView.setAdapter(manageUserAdapter);
        userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AlertDialog.Builder userAlertBuilder = new AlertDialog.Builder(context);
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                user_alert_view = context.getLayoutInflater().inflate(R.layout.manage_user_alertbox,null);
                checkbox_email = (TextView)user_alert_view.findViewById(R.id.manageUserAlert_email);
                checkbox_name = (TextView)user_alert_view.findViewById(R.id.manageUserAlert_Name);
                isAdmin_checkbox = (CheckBox)user_alert_view.findViewById(R.id.is_admin_checkbox);

                checkbox_email.setText(usersList.get(i).getEmail());
                checkbox_name.setText(usersList.get(i).getName());
                userAlertBuilder.setView(user_alert_view);
                userAlertBuilder.show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    class ManageUserAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return usersList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        public void filterList(List<User> filterUser){

        }
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View v = getLayoutInflater().inflate(R.layout.manage_user_item, null);

            mng_email = (TextView)v.findViewById(R.id.mng_user_email);
            nameTxt = (TextView)v.findViewById(R.id.mng_user_name);
            delete_user = (ImageView)v.findViewById(R.id.manageUser_delete);

            mng_email.setText(usersList.get(i).getEmail());
            nameTxt.setText(usersList.get(i).getName());

            delete_user.setOnClickListener(new ManageUserAdapter.imageViewClickListener(i));

            return v;
        }

        class imageViewClickListener implements View.OnClickListener {
            int position;
            public imageViewClickListener( int pos)
            {
                this.position = pos;
            }

            public void onClick(View v) {
                {
                    usersList.remove(position);
                    manageUserAdapter.notifyDataSetChanged();
                }
            }
        }
    }
}
