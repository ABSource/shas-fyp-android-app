package com.developers.pyroid.shas.Fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.developers.pyroid.shas.DTO.Classes.User;
import com.developers.pyroid.shas.DTO.Classes.UserCollection;
import com.developers.pyroid.shas.R;

import java.util.ArrayList;
import java.util.List;

import static com.developers.pyroid.shas.R.id.add_user_btn;


public class ManageUserFragment extends Fragment {

    TextView nameTxt,checkbox_name, mng_email,checkbox_email;

    ImageView delete_user;
    ArrayList<User> Users, usersList;
    ListView userListView;
    View v , user_alert_view;
    Activity context;
    CheckBox isAdmin_checkbox;
    private OnFragmentInteractionListener mListener;

    public ManageUserFragment() {
        // Required empty public constructor
    }

    public static ManageUserFragment newInstance(String param1, String param2) {
        ManageUserFragment fragment = new ManageUserFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_user, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context =getActivity();
        userListView = (ListView)view.findViewById(R.id.manageUserListView);
        UserCollection userCollection = new UserCollection();
        usersList = userCollection.getAllUsers();
        ManageUserAdapter manageUserAdapter = new ManageUserAdapter();
        userListView.setAdapter(manageUserAdapter);
        userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AlertDialog.Builder userAlertBuilder = new AlertDialog.Builder(context);
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                user_alert_view = context.getLayoutInflater().inflate(R.layout.manage_user_alertbox,null);
                checkbox_email = (TextView)user_alert_view.findViewById(R.id.manageUserAlert_email);
                checkbox_name = (TextView)user_alert_view.findViewById(R.id.manageUserAlert_Name);
                isAdmin_checkbox = (CheckBox)user_alert_view.findViewById(R.id.is_admin_checkbox);

                checkbox_email.setText(usersList.get(i).getEmail());
                checkbox_name.setText(usersList.get(i).getName());
                userAlertBuilder.setView(user_alert_view);
                userAlertBuilder.show();
            }
        });


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class ManageUserAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return usersList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        public void filterList(List<User> filterUser){

        }
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View v = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.manage_user_item, null);

            mng_email = (TextView)v.findViewById(R.id.mng_user_email);
            nameTxt = (TextView)v.findViewById(R.id.mng_user_name);
            delete_user = (ImageView)v.findViewById(R.id.manageUser_delete);

            mng_email.setText(usersList.get(i).getEmail());
            nameTxt.setText(usersList.get(i).getName());

            delete_user.setOnClickListener(new imageViewClickListener(i));

            return v;
        }

        class imageViewClickListener implements View.OnClickListener {
            int position;
            public imageViewClickListener( int pos)
            {
                this.position = pos;
            }

            public void onClick(View v) {
                {
                    usersList.remove(position);
                }
            }
        }
    }
}
