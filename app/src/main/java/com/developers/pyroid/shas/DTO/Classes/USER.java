package com.developers.pyroid.shas.DTO.Classes;
import com.developers.pyroid.shas.R;

/**
 * Created by Bilal on 3/18/2018.
 */

public class User {
    int userId;
    String name;
    String email;
    String password;
    String securityQuestion;
    String securtiyAns;
    Integer imageSrc;

    public User(int userId, String name, String email, String password, String securityQuestion, String securtiyAns, Integer imageSrc) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.password = password;
        this.securityQuestion = securityQuestion;
        this.securtiyAns = securtiyAns;
        this.imageSrc = imageSrc;
    }

    public Integer getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(Integer imageSrc) {
        this.imageSrc = imageSrc;
    }


    public String getSecurtiyAns() {
        return securtiyAns;
    }

    public void setSecurtiyAns(String securtiyAns) {
        this.securtiyAns = securtiyAns;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
