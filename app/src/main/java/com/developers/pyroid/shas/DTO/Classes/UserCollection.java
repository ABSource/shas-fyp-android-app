package com.developers.pyroid.shas.DTO.Classes;

import java.util.ArrayList;
import com.developers.pyroid.shas.R;
/**
 * Created by Bilal on 3/18/2018.
 */

public class UserCollection {
    public ArrayList<User> getNewUsers(){
        ArrayList<User> newUserList = new ArrayList<>();
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","My name is khan","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","My name is khan","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","My name is khan","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","My name is khan","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","My name is khan","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","My name is khan","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","My name is khan","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","My name is khan","This is it",R.drawable.ddd ));
        return newUserList;
    }
    public ArrayList<User> getAllUsers(){
        ArrayList<User> newUserList = new ArrayList<>();
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","1","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Ali","ali.basharat@gmail.com","1232","1","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Umar","bilal.arif2917@gmail.com","1232","2","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Usama","bilal.arif2917@gmail.com","1232","1","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Usman","bilal.arif2917@gmail.com","1232","3","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Khan","bilal.arif2917@gmail.com","1232","4","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","4","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Usama","bilal.arif2917@gmail.com","1232","5","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Usman","bilal.arif2917@gmail.com","1232","5","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Khan","bilal.arif2917@gmail.com","1232","7","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","1","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","3","This is it",R.drawable.ddd ));
        newUserList.add(new User(1,"Muhammad Bilal","bilal.arif2917@gmail.com","1232","4","This is it",R.drawable.ddd ));
        return newUserList;
    }


}
