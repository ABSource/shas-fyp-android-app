//package com.developers.pyroid.shas.Fragments;
//
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.developers.pyroid.shas.DTO.Classes.NewUserCollection;
//import com.developers.pyroid.shas.DTO.Classes.Notification;
//import com.developers.pyroid.shas.DTO.Classes.User;
//import com.developers.pyroid.shas.R;
//
//import java.util.ArrayList;
//import java.util.List;
//
//
//public class NewUserFragment extends Fragment {
//
//    ImageView userImg, add_user_btn, remove_user_btn;
//    TextView nameTxt, email_user;
//    ArrayList<User> Users, newUsersList;
//    ListView newUserListView;
//    NewUserAdapter newUserAdapter;
//    View v;
//    public NewUserFragment() {
//        // Required empty public constructor
//    }
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_new_user, container, false);
//        return view;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        newUserListView = (ListView) view.findViewById(R.id.new_user_list);
//
////        Get All New User Requests
//        NewUserCollection newUsers = new NewUserCollection();
//        newUsersList = newUsers.getNewUsers();
////        set Adapter
//        newUserAdapter = new NewUserAdapter();
//        newUserListView.setAdapter(newUserAdapter);
//    }
//
//    class NewUserAdapter extends BaseAdapter {
//
//        @Override
//        public int getCount() {
//            return newUsersList.size();
//        }
//
//        @Override
//        public Object getItem(int i) {
//            return null;
//        }
//        @Override
//        public long getItemId(int i) {
//            return 0;
//        }
//        public void filterList(List<User> filterUser){
//
//        }
//        @Override
//        public View getView(int i, View view, ViewGroup viewGroup) {
//            View v = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.new_user_item, null);
//
//            nameTxt = (TextView)v.findViewById(R.id.new_user_name);
//
//            add_user_btn = (ImageView)v.findViewById(R.id.add_user_btn);
//            remove_user_btn = (ImageView)v.findViewById(R.id.reject_user_btn);
//            email_user = (TextView)v.findViewById(R.id.new_user_email);
//
//            nameTxt.setText(newUsersList.get(i).getName());
//            email_user.setText(newUsersList.get(i).getEmail());
//
//            add_user_btn.setOnClickListener(new imageViewClickListener(i) );
//            remove_user_btn.setOnClickListener(new imageViewClickListener(i));
//            return v;
//        }
//        class imageViewClickListener implements View.OnClickListener {
//            int position;
//            public imageViewClickListener( int pos)
//            {
//                this.position = pos;
//            }
//
//            public void onClick(View v) {
//                {
//                    ImageView image = (ImageView)getItem(position);
//                    if(v.getId() == R.id.add_user_btn){
//                        add_user_btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                        Toast.makeText(v.getContext(), "User is Added Succesfully", Toast.LENGTH_SHORT).show();
//                    }
//                    else
//                        Toast.makeText(v.getContext(), "User is Rejected", Toast.LENGTH_SHORT).show();
//                    newUsersList.remove(position);
//                    newUserAdapter.notifyDataSetChanged();
//                }
//            }
//        }
//    }
//
//
//}
