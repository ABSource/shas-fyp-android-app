package com.developers.pyroid.shas.AdapterClasses;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Bilal on 3/6/2018.
 */
import com.developers.pyroid.shas.R;
public class NotificationHolder extends RecyclerView.ViewHolder {

    ImageView notifImage;
    TextView userNameTxt;
    TextView dateTxt;
    TextView timeTxt;

    public NotificationHolder(View itemView) {
        super(itemView);
        this.notifImage = (ImageView) itemView.findViewById(R.id.notification_img);
        this.userNameTxt = (TextView) itemView.findViewById(R.id.UserNameCaptureImage);
        this.dateTxt = (TextView) itemView.findViewById(R.id.dateCapture);
        this.timeTxt = (TextView) itemView.findViewById(R.id.captureTime);

    }
}
