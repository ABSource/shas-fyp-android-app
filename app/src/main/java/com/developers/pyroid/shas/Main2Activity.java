package com.developers.pyroid.shas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main2Activity extends AppCompatActivity {
    ImageView secQuestionEditBtn, profileEditBtn, password_edit_btn;
    Spinner spinner, set_Pass_Spinner;
    EditText oldAns, newAns, edit_Name, secAns_setPass, set_new_pass;
    Button save_btn_sec, cancel_btn_sec, save_profile_btn,cancel_Prof_btn, save_pass_btn,can_pass_btn;
    TextView txtEmail, txtName, txt_password,security_Quest_TxtView, existingQuestion, errorTxt_name,error_txt_pass,error_txt_security;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    static final String SUCCESS = "success";
    static final String MESSAGE = "message";
    private String _url = "http://192.168.0.100/shas/update_profile.php";


    Context context;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main2);

        profileEditBtn = (ImageView)findViewById(R.id.profile_edit_pencile);

        txtEmail = (TextView) findViewById(R.id.emailProfile);
        txtName = (TextView) findViewById(R.id.fullNameProfile);
        txt_password = (TextView) findViewById(R.id.profilepasswordText);
        security_Quest_TxtView = (TextView)findViewById(R.id.profileSecurityQuestion);

        sharedPref = getSharedPreferences(getString(R.string.SHARED_PREF), MODE_PRIVATE);
        txtEmail.setText(sharedPref.getString("email",""));
        txtName.setText(sharedPref.getString("name",""));
        txt_password.setText(sharedPref.getString("password",""));
        security_Quest_TxtView.setText(sharedPref.getString("answer",""));

        // edit name
        profileEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder editProfileBuilder = new AlertDialog.Builder(Main2Activity.this);
                View edit_prof_view = getLayoutInflater().inflate(R.layout.set_profile_layout,null);
                editProfileBuilder.setTitle("Change Name");

                // Initialize all variables

                edit_Name = (EditText) edit_prof_view.findViewById(R.id.set_input_name);
                errorTxt_name = (TextView)edit_prof_view.findViewById(R.id.errorTxtViewNameEdit);
                save_profile_btn = (Button)edit_prof_view.findViewById(R.id.saveBtnProfile);
                cancel_Prof_btn = (Button)edit_prof_view.findViewById(R.id.cancelBtnProfile);
                final String name = txtName.getText().toString();
                edit_Name.setText(name);
                editProfileBuilder.setView(edit_prof_view);
                final AlertDialog nameDialog = editProfileBuilder.create();
                nameDialog.show();

                save_profile_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(validateName()){
                            if(!edit_Name.getText().toString().equals(name)){
                                final String uName = edit_Name.getText().toString();
                                txtName.setText(uName);

                                editor = sharedPref.edit();
                                editor.putString("name",uName);

                                ArrayList<String> keys = new ArrayList<String>();
                                ArrayList<String> values = new ArrayList<String>();

                                keys.add("updateData"); values.add("name");
                                keys.add("name");       values.add(uName);

                                updateProfile(keys, values);
                            }
                            nameDialog.dismiss();
                        }
                        else
                            requestFocus(txtName);
                    }
                });
                cancel_Prof_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        nameDialog.dismiss();
                    }
                });
            }
        });

        //  Security Question Edit Dialog
        secQuestionEditBtn =(ImageView) findViewById(R.id.profileSecurityQuestEditPencil);

        secQuestionEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder setSecurityQuesBuilder = new AlertDialog.Builder(Main2Activity.this);
                View set_qest_view = getLayoutInflater().inflate(R.layout.set_security_question,null);
                setSecurityQuesBuilder.setTitle("Set Security Question");

                existingQuestion = (TextView) set_qest_view.findViewById(R.id.existing_security_question);
                spinner = (Spinner)set_qest_view.findViewById(R.id.spinner_security_question);
                oldAns = (EditText)set_qest_view.findViewById(R.id.old_security_ans);
                newAns = (EditText)set_qest_view.findViewById(R.id.new_security_ans);
                save_btn_sec = (Button)set_qest_view.findViewById(R.id.saveBtnSecurity);
                cancel_btn_sec = (Button)set_qest_view.findViewById(R.id.cancelBtnSecurity);
                error_txt_security =(TextView) set_qest_view.findViewById(R.id.errorTxtViewQuestion);

                existingQuestion.setText(sharedPref.getString("question",""));
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Main2Activity.this,android.R.layout.simple_spinner_item,
                                                                           getResources().getStringArray(R.array.securityQuestionArray));
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                setSecurityQuesBuilder.setView(set_qest_view);
                final AlertDialog dialog = setSecurityQuesBuilder.create();
                dialog.show();

                save_btn_sec.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(  validateSecurityAns(oldAns,error_txt_security) && validateQuestion(spinner,error_txt_security)
                                && validateSecurityAns(newAns,error_txt_security) ){
//                            security_Quest_TxtView.setText(newAns.getText().toString());
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, _url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject json = new JSONObject(response);
                                        if(json.getInt(SUCCESS) > 0) {
//                                            txt_password.setText(set_new_pass.getText());
                                            editor = sharedPref.edit();
                                            editor.putString("question",spinner.getSelectedItem().toString());
                                            editor.putString("answer", newAns.getText().toString());
                                            editor.commit();
                                            security_Quest_TxtView.setText(newAns.getText().toString());
                                            dialog.dismiss();
                                        }
                                        Toast.makeText(Main2Activity.this, json.getString(MESSAGE), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener(){
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(Main2Activity.this, error.toString(), Toast.LENGTH_LONG).show();
                                }
                            }
                            )
                            {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();

                                    params.put("uID", String.valueOf(sharedPref.getInt("uID",0)));
                                    params.put("updateData","question");
                                    params.put("oldAns",oldAns.getText().toString()); //set_Pass_Spinner.getSelectedItem().toString()
                                    params.put("newQuest",spinner.getSelectedItem().toString());
                                    params.put("newAns",newAns.getText().toString());

                                    return params;
                                }
                            };
                            //Adding stringRequest to requestQueue
                            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
                        }
                    }
                });
                cancel_btn_sec.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

        // password change dialog
        password_edit_btn = (ImageView)findViewById(R.id.change_password_pencil);
        password_edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder pass_change_builder = new AlertDialog.Builder(Main2Activity.this);
                final View pass_dialog_view = getLayoutInflater().inflate(R.layout.set_password_layout,null);
                pass_change_builder.setTitle("Reset Password");

                set_Pass_Spinner = (Spinner) pass_dialog_view.findViewById(R.id.setPassword_secQuesSpinner);
                secAns_setPass = (EditText)pass_dialog_view.findViewById(R.id.security_answer_setpassword);
                set_new_pass = (EditText)pass_dialog_view.findViewById(R.id.input_password_new_changePass);
                save_pass_btn = (Button)pass_dialog_view.findViewById(R.id.saveBtnPassword);
                can_pass_btn = (Button)pass_dialog_view.findViewById(R.id.cancelBtnPassword);
                error_txt_pass =(TextView) pass_dialog_view.findViewById(R.id.errorTxtViewResetPass_text);
                ArrayAdapter<String> Password_adapter = new ArrayAdapter<String>(Main2Activity.this,android.R.layout.simple_spinner_item,
                        getResources().getStringArray(R.array.securityQuestionArray));
                Password_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                set_Pass_Spinner.setAdapter(Password_adapter);

                pass_change_builder.setView(pass_dialog_view);
                final AlertDialog passworddialog = pass_change_builder.create();
                passworddialog.show();

                save_pass_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(validateQuestion(set_Pass_Spinner,error_txt_pass) && validateSecurityAns(secAns_setPass,error_txt_pass) && validatePassword(set_new_pass)){
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, _url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject json = new JSONObject(response);
                                        if(json.getInt(SUCCESS) > 0) {
//                                            txt_password.setText(set_new_pass.getText());
                                            editor = sharedPref.edit();
                                            editor.putString("password", set_new_pass.getText().toString());
                                            editor.commit();
                                            txt_password.setText(set_new_pass.getText());
                                            passworddialog.dismiss();
                                        }
                                        Toast.makeText(Main2Activity.this, json.getString(MESSAGE), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener(){
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(Main2Activity.this, error.toString(), Toast.LENGTH_LONG).show();
                                }
                            }
                            )
                            {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();

                                    params.put("uID", String.valueOf(sharedPref.getInt("uID",0)));
                                    params.put("updateData","password");
                                    params.put("password",set_new_pass.getText().toString());
                                    params.put("question",set_Pass_Spinner.getSelectedItem().toString());
                                    params.put("answer",secAns_setPass.getText().toString());

                                    return params;
                                }
                            };
                            //Adding stringRequest to requestQueue
                            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
                        }
                    }
                });
                can_pass_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        passworddialog.dismiss();
                    }
                });
            }
        });

    }

    private void updateProfile(final ArrayList<String> keys, final ArrayList<String> values){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, _url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Toast.makeText(Main2Activity.this, json.getString(MESSAGE), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Main2Activity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("uID", String.valueOf(sharedPref.getInt("uID",0)));
                for(int i = 0; i < keys.size(); i++){
                    params.put(keys.get(i), values.get(i));
                }

                return params;
            }
        };
        //Adding stringRequest to requestQueue
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private boolean validateQuestion(Spinner spinner ,TextView error_txt){
        if(spinner.getSelectedItem().toString().equals("Select your security question...")){
            error_txt.setText(R.string.vallidSecurityQuestion);
            error_txt.setVisibility(View.VISIBLE);
            return false;
        }
        else
            error_txt.setVisibility(View.INVISIBLE);
        return true;
    }
    private boolean validateSecurityAns(EditText text, TextView error_txt) {
        String ans = text.getText().toString();

        if (ans.isEmpty()) {
            error_txt.setText(R.string.vallidSecurityAns);
            error_txt.setVisibility(View.VISIBLE);
            text.requestFocus();
            return false;
        } else {
            error_txt.setVisibility(View.INVISIBLE);
        }
        return true;
    }
    private boolean validateName() {
        if (edit_Name.getText().toString().trim().isEmpty()) {
            errorTxt_name.setText(R.string.err_msg_name);
            errorTxt_name.setVisibility(View.VISIBLE);
            requestFocus(txtName);
            return false;
        } else {
            errorTxt_name.setVisibility(View.INVISIBLE);

        }

        return true;
    }

    private boolean validatePassword(EditText pass) {
        if (pass.getText().toString().trim().isEmpty()) {
            //inputPassword.setError(getString(R.string.err_msg_password));
            error_txt_pass.setText(R.string.err_msg_password);
            error_txt_pass.setVisibility(View.VISIBLE);
            requestFocus(pass);
            return false;
        } else {

        }

        return true;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
