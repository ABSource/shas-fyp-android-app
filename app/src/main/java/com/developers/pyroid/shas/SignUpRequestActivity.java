package com.developers.pyroid.shas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.developers.pyroid.shas.DTO.Classes.NewUserCollection;
import com.developers.pyroid.shas.DTO.Classes.SignupRequest;
import com.developers.pyroid.shas.DTO.Classes.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SignUpRequestActivity extends AppCompatActivity {
    ImageView userImg, add_user_btn, remove_user_btn;
    TextView nameTxt, email_signup;
    ArrayList<SignupRequest> newUsersList;
    ListView newUserListView;
    NewUserAdapter newUserAdapter;
    View v;
    SignupRequest[] requests;
    private String _url = "http://192.168.0.100/shas/get_signup_requests.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_request);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle("Signup Requests");
        newUserListView = (ListView)findViewById(R.id.new_user_list);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, _url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response", response);
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();

                requests = gson.fromJson(response, SignupRequest[].class);

//        Get All New User Requests
                NewUserCollection newUsers = new NewUserCollection();
                newUsersList = newUsers.getNewUsers(requests);
//        set Adapter
                newUserAdapter = new NewUserAdapter();
                newUserListView.setAdapter(newUserAdapter);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SignUpRequestActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        });
        //Adding stringRequest to requestQueue
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private String getSignUpRequests(){

        InputStream is;
        String responde = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(_url);

        try {
            HttpResponse response = httpclient.execute(httppost);

            if(response != null){
                is = response.getEntity().getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();

                String line = null;
                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                responde = sb.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return responde;
    }
    class NewUserAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return newUsersList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v = getLayoutInflater().inflate(R.layout.new_user_item, null);

            nameTxt = (TextView) v.findViewById(R.id.new_user_name);
            email_signup = (TextView)v.findViewById(R.id.new_user_email);
            add_user_btn = (ImageView) v.findViewById(R.id.add_user_btn);
            remove_user_btn = (ImageView) v.findViewById(R.id.reject_user_btn);

            email_signup.setText(newUsersList.get(i).getEmail());
            nameTxt.setText(newUsersList.get(i).getName());

            add_user_btn.setOnClickListener(new NewUserAdapter.imageViewClickListener(i));
            remove_user_btn.setOnClickListener(new NewUserAdapter.imageViewClickListener(i));
            return v;
        }

        class imageViewClickListener implements View.OnClickListener {
            int position;

            public imageViewClickListener(int pos) {
                this.position = pos;
            }

            public void onClick(View v) {
                {
                    ImageView image = (ImageView) getItem(position);
                    if (v.getId() == R.id.add_user_btn) {
                        add_user_btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        Toast.makeText(v.getContext(), "User is Added Succesfully", Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(v.getContext(), "User is Rejected", Toast.LENGTH_SHORT).show();
                    newUsersList.remove(position);
                    newUserAdapter.notifyDataSetChanged();
                }
            }
        }
    }
}
